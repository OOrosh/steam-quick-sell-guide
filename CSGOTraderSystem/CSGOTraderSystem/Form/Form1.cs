﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zayko.Finance;
using MetroFramework.Forms;
using MetroFramework.Controls;

namespace CSGOTraderSystem
{
    public partial class Form1 : MetroForm
    {
        
        CurrencyConverter myCurrencyConverter = new CurrencyConverter();
        double keyVal = 2.2d;
        int numberOfDecimals = 1;
        bool change = false;
        string[] cbFromData;
        string[] cbToData;

        public Form1()
        {
            cbFromData = CurrencyList.Codes.ToArray<string>();
            cbToData = CurrencyList.Codes.ToArray<string>();
            
            InitializeComponent();

            comboBoxTo.DataSource = cbToData;
            comboBoxFrom.DataSource = cbFromData;

            comboBoxFrom.SelectedItem = "EUR";
            comboBoxTo.SelectedItem = "USD";
            metroComboBoxColor.SelectedItem = "Default";

            convertFromTo();

            this.comboBoxTo.SelectedIndexChanged += new System.EventHandler(this.comboBoxTo_SelectedIndexChanged);
            this.comboBoxFrom.SelectedIndexChanged += new System.EventHandler(this.comboBoxFrom_SelectedIndexChanged);
            this.metroComboBoxColor.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxColor_SelectedIndexChanged);

            styleChanged();
        }
        
        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            double item, keys, percentage;

            if (double.TryParse(textBoxItem.Text, out item) &&
                double.TryParse(textBoxKeys.Text, out keys) &&
                double.TryParse(Percentage.Text, out percentage))
            {
                textBoxItemOut.Text = Convert.ToString(Math.Round(item * (percentage / 100d), 2));
                textBoxKeysOut.Text = Convert.ToString(Math.Round(keys * (percentage / 100d), 2));

                textBoxMarketItem.Text = Convert.ToString(Math.Round(item / 1.15d - 0.01, 2));
                textBoxMarketKeys.Text = Convert.ToString(Math.Round(keys / 1.15d - 0.01, 2));
            }
        }

        //SETTINGS----------------------------------

        bool settings = true;
        SettingsInterface si;

        private void Form1_Load(object sender, EventArgs e)
        {
            if (settings)
            {
                si = new SettingsInterface(this);

                si.addProperty(ref metroToggleNightMode);
                si.addProperty(ref metroToggleScreenLock);

                si.addProperty(ref rbEUR);
                si.addProperty(ref rbUSD);
                si.addProperty(ref rbGBP);
                si.addProperty(ref rbOther);

                si.addProperty(ref metroComboBoxColor);
                si.addProperty(ref comboBoxFrom);
                si.addProperty(ref comboBoxTo);

                si.addProperty(ref this.textBoxSteamID);
                si.addProperty(ref this.textBoxFrom);
                //si.addProperty(ref this.textBoxTo);

                si.addProperty(ref this.textBoxKeyVal);

                si.addProperty(ref this.textBoxItem);
                si.addProperty(ref this.textBoxKeys);
                si.addProperty(ref this.Percentage);
                si.addProperty(ref this.textBoxItemOut);
                si.addProperty(ref this.textBoxKeysOut);
                si.addProperty(ref this.textBoxMarketItem);
                si.addProperty(ref this.textBoxMarketKeys);

                si.addProperty(ref this.metroTextBoxOP);
                //si.addProperty(ref this.metroTextBoxPP);

                si.checkSettings(this);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (settings) si.save();
        }

        //-----------------

        private void textBoxKeys_TextChanged(object sender, EventArgs e)
        {
            if (textBoxKeys.Text != "" && change == false)
            {
                change = true;
                textBoxItem.Text = Convert.ToString(Math.Round(Convert.ToDouble(textBoxKeys.Text) * keyVal, numberOfDecimals));
            }
            else
            {
                change = false;
            }
        }

        private void textBoxItem_TextChanged(object sender, EventArgs e)
        {
            if (textBoxItem.Text != "" && change == false)
            {
                change = true;
                textBoxKeys.Text = Convert.ToString(Math.Round(Convert.ToDouble(textBoxItem.Text) / keyVal, numberOfDecimals));
            }
            else
            {
                change = false;
            }
            
        }

        private void textBoxP_TextChanged(object sender, EventArgs e)
        {

        }

        //------------------------------------------------------------------------------
        
        private void rbUSD_CheckedChanged(object sender, EventArgs e)
        {
            keyVal = 2.5;
            enableEnterKeyPrice(false);
            changePriceFromKeysToItem();
        }

        private void rbGBP_CheckedChanged(object sender, EventArgs e)
        {
            keyVal = 1.9;
            enableEnterKeyPrice(false);
            changePriceFromKeysToItem();
        }

        private void rbEUR_CheckedChanged(object sender, EventArgs e)
        {
            keyVal = 2.2;
            enableEnterKeyPrice(false);
            changePriceFromKeysToItem();
        }

        private void rbOther_CheckedChanged(object sender, EventArgs e)
        {
            enableEnterKeyPrice(true);
            changePriceFromKeysToItem();
        }

        void enableEnterKeyPrice(bool val)
        {
            textBoxKeyVal.Enabled = val;
            labelEnterKeyPrice.Enabled = val;
            if (val)
            {
                if (double.TryParse(textBoxKeyVal.Text, out keyVal)) changePriceFromKeysToItem();
            }
        }
        //TODO: try parse
        private void textBoxKeyVal_TextChanged(object sender, EventArgs e)
        {
            if (textBoxKeyVal.Enabled)
            {
                keyVal = Convert.ToDouble(textBoxKeyVal.Text);
                changePriceFromKeysToItem();
            }
        }

        void changePriceFromKeysToItem()
        {
            double key;
            if (double.TryParse(textBoxKeys.Text, out key))
            {
                textBoxItem.Text = Convert.ToString(Math.Round(key * keyVal, numberOfDecimals));
            }

        }


        //-----------------------------------------------------------------------------------------

        CurrencyData cd;
        bool converted = false;
        bool lastEdited = false;//from = false  to = true
        double from;

        private void comboBoxFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lastEdited) convertToFrom();
            else convertFromTo();
        }

        private void comboBoxTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lastEdited) convertToFrom();
            else convertFromTo();
        }

        private void textBoxFrom_TextChanged(object sender, EventArgs e)
        {
            convertFromTo();
        }

        private void textBoxTo_TextChanged(object sender, EventArgs e)
        {
            convertToFrom();
        }

        void convertFromTo()
        {
            
            if (converted == false && double.TryParse(textBoxFrom.Text, out from))
            {
                converted = true;
                lastEdited = false;
                //ob.AppendText(comboBoxFrom.SelectedItem.ToString());
                //ob.AppendText(comboBoxTo.SelectedItem.ToString());
                cd = new CurrencyData(comboBoxFrom.SelectedItem.ToString(), comboBoxTo.SelectedItem.ToString());
                try
                {
                    myCurrencyConverter.GetCurrencyData(ref cd);

                }
                catch
                {

                }
                textBoxTo.Text = Convert.ToString(from * cd.Rate);
                
            }
            else converted = false;
        }

        void convertToFrom()
        {
            if (converted == false && double.TryParse(textBoxTo.Text, out from))
            {
                try
                {
                    converted = true;
                    lastEdited = true;

                    cd = new CurrencyData(comboBoxTo.SelectedItem.ToString(), comboBoxFrom.SelectedItem.ToString());
                    try
                    {
                        myCurrencyConverter.GetCurrencyData(ref cd);

                    }
                    catch
                    {

                    }
                    textBoxFrom.Text = Convert.ToString(from * cd.Rate);
                }
                    catch (Exception)
                {

                }
                
            }
            else converted = false;
        }

        private void buttonSwitch_Click(object sender, EventArgs e)
        {
            string cbF, cbT;

            cbF = comboBoxTo.SelectedItem.ToString();
            cbT = comboBoxFrom.SelectedItem.ToString();

            comboBoxFrom.SelectedItem = cbF;
            comboBoxTo.SelectedItem = cbT;
        }

        //--------------------------------------------------------------------

        void styleChanged(object sender, EventArgs e)
        {
            styleChanged();
        }

        void styleChanged()
        {
            if (metroStyleManager1.Theme.ToString() == "Light")
            {
                changeColor(Color.White);
            }
            if (metroStyleManager1.Theme.ToString() == "Dark")
            {
                changeColor(Color.FromArgb(255, 18, 18, 18));
            }
        }

        void changeColor(Color color)
        {
            foreach (Control element in GetAll(this, typeof(TableLayoutPanel)))
            {
                element.BackColor = color;
            }
        }

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        //-------------------------------------------------------------------------------

        void metroComboBoxColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroStyleManager1.Style = (MetroFramework.MetroColorStyle)metroComboBoxColor.SelectedIndex;
        }

        private void metroToggleNightMode_CheckedChanged(object sender, EventArgs e)
        {
            if (metroToggleNightMode.Checked == true)
            {
                metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Dark;
            }
            else metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Light;
            
        }

        private void metroToggleScreenLock_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = metroToggleScreenLock.Checked;
        }
    }
}
