﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Web.Script.Serialization;
namespace CSGOTraderSystem
{
    class SteamNameToID
    {
        public SteamNameToID()
        {

        }

        public string getID(string steamName)
        {
            string file = new WebClient().DownloadString("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=DCD148674953187B729DEBCC07497510&vanityurl=" + steamName);
            JavaScriptSerializer js = new JavaScriptSerializer();
            JSON r = js.Deserialize<JSON>(file);
            if (r.response.success == 1)
            {
                return r.response.steamid.ToString();
            }
            return "";

        }

        public string getAvatarFull(string steamid)
        {
            string file = new WebClient().DownloadString("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=DCD148674953187B729DEBCC07497510&steamids=" + steamid);
            JavaScriptSerializer js = new JavaScriptSerializer();
            JSON r = js.Deserialize<JSON>(file);
            return r.response.players[0].avatarfull;
        }
    }

    class JSON
    {
        public ResultName response { get; set; }
    }

    class ResultName
    {
        public Int64 steamid { get; set; }
        public int success { get; set; }
        public Player[] players { get; set; }
    }

    class Player
    {
        public string avatarfull { get; set; }
    }
}
