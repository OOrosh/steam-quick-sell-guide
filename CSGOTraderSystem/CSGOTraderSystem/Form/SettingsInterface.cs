﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework.Controls;
using System.Configuration;
using System.Resources;
using System.Diagnostics;

namespace CSGOTraderSystem
{
    class SettingsInterface
    {
        public List<MTBT> mtbt = new List<MTBT>();
        public List<MTC> bools = new List<MTC>();
        public List<CBT> cbts = new List<CBT>();
        public List<RBC> rbcs = new List<RBC>();

        Settings settings;
        JavaScriptSerializer js;
        ResourceManager rm;

        Form form;

        string path;

        //FIX : create new settings
        public SettingsInterface(Form form)
        {
            this.form = form;
            
            rm = Properties.Resources.ResourceManager;
            js = new JavaScriptSerializer();

            path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/TradeBro";

            if (!Directory.Exists(path) || !File.Exists(path + "/settings.cfg"))
            {
                Directory.CreateDirectory(path);
                FileStream fs = System.IO.File.Create(path + "/settings.cfg");
                fs.Close();
            }
            read();
        }

        private void read()
        {
            StreamReader sr;
            sr = new StreamReader(path + "/settings.cfg");
            settings = js.Deserialize<Settings>(sr.ReadToEnd());
            sr.Close();
        }

        public void addProperty(ref MetroTextBox c)
        {
            mtbt.Add(new MTBT(ref c));
        }

        public void addProperty(ref MetroToggle c)
        {
            bools.Add(new MTC(ref c));

        }

        public void addProperty(ref MetroComboBox c)
        {
            cbts.Add(new CBT(ref c));
        }

        public void addProperty(ref MetroRadioButton c)
        {
            rbcs.Add(new RBC(ref c));
        }

        public void checkSettings(Control form)
        {
            //textboxes
            foreach (MTBT item in mtbt)
            {
                if (settings == null)
                {
                    save();
                }
                item.val = settings.getTextSetting(item.name);
                if (item.val == null)
                {
                    item.val = getText(form, item);
                }
                else
                {
                    setText(form, item);
                }
            }

            //toggles
            foreach (MTC item in bools)
            {
                if (settings == null)
                {
                    save();
                }
                item.val = settings.getBoolSetting(item.name);
                item.getControl().Checked = item.val;
            }

            //combo boxes
            foreach (CBT item in cbts)
            {
                if (settings == null)
                {
                    save();
                }
                item.val = settings.getCBTTextSetting(item.name);
                if (item.val == null)
                {
                    item.val = item.getControl().Text;
                }
                else
                {
                    item.getControl().Text = item.val;
                }
            }

            //radio buttons
            foreach (RBC item in rbcs)
            {
                if (settings == null)
                {
                    save();
                }
                item.val = settings.getRBCBoolSetting(item.name);
                item.getControl().Checked = item.val;
            }
        }

        private void setText(Control form, MTBT item)
        {
            item.getControl().Text = item.val;
        }

        private string getText(Control form, MTBT item)
        {
            return item.getControl().Text;
        }

        /*
        private void setToggleState(Control form, Bool item)
        {
            foreach (MetroToggle i in GetAll(form, typeof(MetroToggle)))
            {
                if (i.Name == item.name)
                {
                    i.Checked = item.val;
                }
            }
        }

        private void getToggleState(Control form, Bool item)
        {
            foreach (MetroToggle i in GetAll(form, typeof(MetroToggle)))
            {
                if (i.Name == item.name)
                {
                    i.Checked = item.val;
                }
            }
        }
        */

        public void save()
        {
            settings = new Settings();

            //text boxes
            foreach (MTBT item in mtbt)
            {
                item.val = getText(form, item);
            }
            settings.mtbt = mtbt.ToArray();

            //toggles
            foreach (MTC item in bools)
            {
                item.val = item.getControl().Checked;
            }
            settings.mtc = bools.ToArray();

            //combo boxes
            foreach (CBT item in cbts)
            {
                item.val = item.getControl().Text;
            }
            settings.cbts = cbts.ToArray();

            //radio buttons
            foreach (RBC item in rbcs)
            {
                item.val = item.getControl().Checked;
            }
            settings.rbcs = rbcs.ToArray();

            StreamWriter sw = new StreamWriter(path + "/settings.cfg");
            //Properties.Resources.SettingsJSON.Replace(Properties.Resources.SettingsJSON, js.Serialize(settings));
            Debug.WriteLine(js.Serialize(settings));
            sw.Write(js.Serialize(settings));
            sw.Close();
        }

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
    }

    

    
    public class Settings
    {
        public MTBT[] mtbt { get; set; }
        public MTC[] mtc { get; set; }
        public CBT[] cbts { get; set; }
        public RBC[] rbcs { get; set; }

        public bool getBoolSetting(string name)
        {
            foreach (MTC item in mtc)
            {
                if (item.name == name)
                {
                    return item.val;
                }
            }
            return false;
        }

        public string getTextSetting(string name)
        {
            foreach (MTBT item in mtbt)
            {
                if (item.name == name)
                {
                    return item.val;
                }
            }
            return "";
        }

        public bool getRBCBoolSetting(string name)
        {
            foreach (RBC item in rbcs)
            {
                if (item.name == name)
                {
                    return item.val;
                }
            }
            return false;
        }

        public string getCBTTextSetting(string name)
        {
            foreach (CBT item in cbts)
            {
                if (item.name == name)
                {
                    return item.val;
                }
            }
            return "";
        }

    }

    public class MTBT
    {
        public string name { get; set; }
        public string val { get; set; }
        private MetroTextBox c;

        public MTBT(string name, string val, ref MetroTextBox c)
        {
            this.name = name;
            this.val = val;
            this.c = c;
        }

        public MTBT(ref MetroTextBox c)
        {
            this.name = c.Name;
            this.val = c.Text;
            this.c = c;
        }

        public MetroTextBox getControl()
        {
            return c;
        }

        public MTBT()
        {
            
        }
    }

    public class MTC
    {
        public string name { get; set; }
        public bool val { get; set; }
        private MetroToggle c;

        public MTC(string name, bool val, ref MetroToggle c)
        {
            this.name = name;
            this.val = val;
            this.c = c;
        }

        public MTC(ref MetroToggle c)
        {
            this.name = c.Name;
            this.val = c.Checked;
            this.c = c;
        }

        public MetroToggle getControl()
        {
            return c;
        }

        public MTC()
        {

        }
    }

    public class CBT
    {
        public string name { get; set; }
        public string val { get; set; }
        private MetroComboBox c;

        public CBT(string name, string val, ref MetroComboBox c)
        {
            this.name = name;
            this.val = val;
            this.c = c;
        }

        public CBT(ref MetroComboBox c)
        {
            this.name = c.Name;
            this.val = c.Text;
            this.c = c;
        }

        public MetroComboBox getControl()
        {
            return c;
        }

        public CBT()
        {

        }
    }

    public class RBC
    {
        public string name { get; set; }
        public bool val { get; set; }
        private MetroRadioButton c;

        public RBC(string name, bool val, ref MetroRadioButton c)
        {
            this.name = name;
            this.val = val;
            this.c = c;
        }

        public RBC(ref MetroRadioButton c)
        {
            this.name = c.Name;
            this.val = c.Checked;
            this.c = c;
        }

        public MetroRadioButton getControl()
        {
            return c;
        }

        public RBC()
        {

        }
    }
}
