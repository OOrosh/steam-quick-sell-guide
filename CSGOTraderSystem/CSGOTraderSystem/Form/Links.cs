﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace CSGOTraderSystem
{
    public partial class Form1
    {

        private void toolStripButtonBitSkins_Click(object sender, EventArgs e)
        {
            Process.Start("https://bitskins.com/");
        }

        private void toolStripButtonOPSkins_Click(object sender, EventArgs e)
        {
            Process.Start("https://opskins.com/");
        }

        private void toolStripButtonStash_Click(object sender, EventArgs e)
        {
            Process.Start("https://csgostash.com/");
        }

        private void toolStripButtonRep_Click(object sender, EventArgs e)
        {
            Process.Start("https://steamrep.com/");
        }

        private void toolStripButtonSIH_Click(object sender, EventArgs e)
        {
            Process.Start("https://chrome.google.com/webstore/detail/steam-inventory-helper/cmeakgjggjdlcpncigglobpjbkabhmjl");
        }

        private void toolStripButtonBot_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.reddit.com/message/compose?to=ShubblerBot&subject=Request&message=have%2Fwant%20string");
        }

        private void toolStripButtonReddit_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.reddit.com/r/GlobalOffensiveTrade/");
        }

        private void toolStripButtonLounge_Click(object sender, EventArgs e)
        {
            Process.Start("https://csgolounge.com/");
        }

        private void toolStripButtonExchange_Click(object sender, EventArgs e)
        {
            Process.Start("https://csgo.exchange/");
        }

        private void toolStripButtonZone_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.csgozone.net/");
        }

        private void toolStripButtonMetjm_Click(object sender, EventArgs e)
        {
            Process.Start("http://metjm.net/csgo/");
        }

        private void toolStripButtonCSGOAnalyst_Click(object sender, EventArgs e)
        {
            Process.Start("http://csgo.steamanalyst.com/");
        }

        private void toolStripButtonMarket_Click(object sender, EventArgs e)
        {
            Process.Start("http://steamcommunity.com/market/");
        }



        private void metroLink1_Click(object sender, EventArgs e)
        {
            Process.Start("http://steamcommunity.com/id/JuanTheGod/");
        }

        private void metroLink2_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.reddit.com/user/Mand1ch/");
        }

        private void metroLink3_Click(object sender, EventArgs e)
        {
            Process.Start("http://steamcommunity.com/id/oorosh/");
        }

        private void metroLink6_Click(object sender, EventArgs e)
        {
            Process.Start("http://steamcommunity.com/id/blejachn1/");
        }

        private void metroLink7_Click(object sender, EventArgs e)
        {
            Process.Start("http://steamcommunity.com/id/SummerV3/");
        }

        private void metroLink4_Click(object sender, EventArgs e)
        {
            Process.Start("http://steamcommunity.com/id/chompston/");
        }

        private void metroLink5_Click(object sender, EventArgs e)
        {
            Process.Start("http://steamcommunity.com/profiles/harambe123123/");
        }
    }
}
