﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using System.Net;
using System.IO;

namespace CSGOTraderSystem
{
    partial class Form1
    {
        SteamNameToID snti;
        JSONFloatReader jfr;
        string steamid;
        Item[] items;
        ItemControl[] controls;
        List<ItemImage> icons = new List<ItemImage>();

        Thread loadingThread;

        private delegate void AddItemControl(int i);
        private delegate void RemoveItemControl(int i);
        private delegate void ItemControlSetVisible(int i, bool b);
        private delegate void ItemControlSetWearText(int i, string text);
        private delegate void ItemControlSetExtText(int i, string text);
        private delegate void ItemControlExtSetVisible(int i, bool b);
        private delegate void ItemControlSetPicture(int i, Image image);
        private delegate void ItemControlSetPictureTooltip(int i, string s);
        private delegate void SetPlayerAvatar(Image image);
        private delegate void ClearFlowLayout();
        //TODO: Add steam api server status checker

        private void buttonCheckFloat_Click(object sender, EventArgs e)
        {

            loadingThread = new Thread(loadItems);
            loadingThread.Start();  
        }

        public void loadItems()
        {
            clearFlowLayout();

            if (controls != null)
            {
                for (int i = 0; i < controls.Length; i++)
                {
                    removeItemControl(i);
                }
            }
            //TODO: ADD item image array
            snti = new SteamNameToID();
            jfr = new JSONFloatReader();
            steamid = snti.getID(textBoxSteamID.Text);
            if (steamid != "")
            {
                Thread ta = new Thread(() => downloadPlayerAvatar(snti.getAvatarFull(steamid)));
                ta.Start();
                items = jfr.ReadJson(Convert.ToInt64(steamid));
                if (items != null)
                {
                    controls = new ItemControl[items.Length];

                    for (int i = 0; i < items.Length; i++)
                    {
                        controls[i] = new ItemControl();
                        //flowLayoutPanelInventory.Controls.Add(controls[i]);
                        addItemControl(i);

                        if (items[i].getWare() == -1)
                        {
                            //controls[i].labelWear.Text = "";
                            itemControlSetWearText(i, "");
                            //controls[i].labelExt.BorderStyle = System.Windows.Forms.BorderStyle.None;
                            itemControlExtSetVisible(i, false);
                        }
                        else //controls[i].labelWear.Text = Math.Round(items[i].getWare(), 6).ToString();
                            itemControlSetWearText(i, Math.Round(items[i].getWare(), 6).ToString());
                        //controls[i].labelExt.Text = checkExt(items[i].getWare());
                        itemControlSetExtText(i, checkExt(items[i].getWare()));
                        itemControlSetVisible(i, true);
                        itemControlSetPictureTooltip(i, " name : " + jfr.ReadItemName(textBoxSteamID.Text, items[i].id) + "\r\n " + "Pattern : " + Math.Round(items[i].getPattern(), 0).ToString());
                        string url = "http://steamcommunity-a.akamaihd.net/economy/image/" + jfr.ReadImageURL(textBoxSteamID.Text, items[i].id);
                        Thread t = new Thread(() => downloadImage(i - 1, url, items[i - 1].id));
                        t.Start();
                        //controls[i].pictureBox1.Image = image;

                        //controls[i].Visible = true;

                    }
                }
                
            }
            
        }

        void downloadImage(int i, string url, Int64 id)
        {
            int _i = i;
            string _url = url;

            foreach (ItemImage item in icons)
            {
                if (item.id == id)
                {
                    itemControlSetPicture(_i, item.image);
                    return;
                }
            }

            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(_url);
            MemoryStream ms = new MemoryStream(bytes);
            
            try
            {
                icons.Add(new ItemImage(id, System.Drawing.Image.FromStream(ms)));
                itemControlSetPicture(_i, icons[icons.Count - 1].image);
                //image.Dispose();
            }
            catch (Exception)
            {
                
            }
            bytes = new byte[0];
            wc.Dispose();
            ms.Dispose();
            return;
        }

        void downloadPlayerAvatar(string url)
        {
            string _url = url;
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(_url);
            MemoryStream ms = new MemoryStream(bytes);
            Image image;

            try
            {
                image = System.Drawing.Image.FromStream(ms);
                setPlayerAvatar(image);
            }
            catch (Exception)
            {

            }

            return;
        }

        void itemControlSetVisible(int i, bool b)
        {
            if (this.controls[i].InvokeRequired)
            {
                ItemControlSetVisible d = new ItemControlSetVisible(itemControlSetVisible);
                this.Invoke(d, new object[] { i, b });
            }
            else
            {
                controls[i].Visible = b;
            }
        }

        void itemControlSetWearText(int i, string text)
        {
            if (this.flowLayoutPanelInventory.InvokeRequired)
            {
                ItemControlSetWearText d = new ItemControlSetWearText(itemControlSetWearText);
                this.Invoke(d, new object[] { i, text });
            }
            else
            {
                controls[i].labelWear.Text = text;
            }
        }

        void itemControlSetExtText(int i, string text)
        {
            if (this.flowLayoutPanelInventory.InvokeRequired)
            {
                ItemControlSetExtText d = new ItemControlSetExtText(itemControlSetExtText);
                this.Invoke(d, new object[] { i, text });
            }
            else
            {
                controls[i].labelExt.Text = text;
            }
        }

        void itemControlExtSetVisible(int i, bool b)
        {
            if (this.flowLayoutPanelInventory.InvokeRequired)
            {
                ItemControlSetVisible d = new ItemControlSetVisible(itemControlExtSetVisible);
                this.Invoke(d, new object[] { i, b });
            }
            else
            {
                controls[i].labelExt.Visible = b;
            }
        }
        //TODO: fix
        void itemControlSetPicture(int i, Image image)
        {
            if (this.controls[i].InvokeRequired)
            {
                ItemControlSetPicture d = new ItemControlSetPicture(itemControlSetPicture);
                this.Invoke(d, new object[] { i, image });
            }
            else
            {
                controls[i].pictureBox1.Image = image;
            }
        }

        void itemControlSetPictureTooltip(int i, string s)
        {
            if (this.controls[i].InvokeRequired)
            {
                ItemControlSetPictureTooltip d = new ItemControlSetPictureTooltip(itemControlSetPictureTooltip);
                this.Invoke(d, new object[] { i, s });
            }
            else
            {
                this.toolTip1.SetToolTip(controls[i].pictureBox1, s);
            }
        }

        void setPlayerAvatar(Image image)
        {
            if (this.Avatar.InvokeRequired)
            {
                SetPlayerAvatar d = new SetPlayerAvatar(setPlayerAvatar);
                this.Invoke(d, new object[] { image });
            }
            else
            {
                this.Avatar.Image = image;
            }
        }

        void addItemControl(int i)
        {
            if (this.flowLayoutPanelInventory.InvokeRequired)
            {
                AddItemControl d = new AddItemControl(addItemControl);
                this.Invoke(d, new object[] { i });
            }
            else
            {
                flowLayoutPanelInventory.Controls.Add(controls[i]);
            }
        }

        void removeItemControl(int i)
        {
            if (this.flowLayoutPanelInventory.InvokeRequired)
            {
                RemoveItemControl d = new RemoveItemControl(removeItemControl);
                this.Invoke(d, new object[] { i });
            }
            else
            {
                flowLayoutPanelInventory.Controls.Remove(controls[i]);
                controls[i].Dispose();
            }
        }

        void clearFlowLayout()
        {
            if (this.flowLayoutPanelInventory.InvokeRequired)
            {
                ClearFlowLayout d = new ClearFlowLayout(clearFlowLayout);
                this.Invoke(d, new object[] { });
            }
            else
            {
                flowLayoutPanelInventory.Controls.Clear();
            }
        }

        string checkExt(double wear)
        {
            if (wear >= 0 && wear < 0.07)
                return "FN";
            if (wear >= 0.07 && wear < 0.15)
                return "MW";
            if (wear >= 0.15 && wear < 0.38 )
                return "FT";
            if (wear >= 0.38 && wear < 0.44)
                return "WW";
            if (wear >= 0.44 && wear < 1)
                return "BS";

            return "";
        }
    }

    class ItemImage
    {
        public Int64 id;
        public Image image;

        public ItemImage(Int64 id, Image image)
        {
            this.id = id;
            this.image = image;
        }
    }
}
