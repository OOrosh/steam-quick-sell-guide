﻿namespace CSGOTraderSystem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

       #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonSwitch = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.comboBoxFrom = new MetroFramework.Controls.MetroComboBox();
            this.comboBoxTo = new MetroFramework.Controls.MetroComboBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxFrom = new MetroFramework.Controls.MetroTextBox();
            this.textBoxTo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.metroPanel4 = new MetroFramework.Controls.MetroPanel();
            this.labelEnterKeyPrice = new MetroFramework.Controls.MetroLabel();
            this.textBoxKeyVal = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.rbEUR = new MetroFramework.Controls.MetroRadioButton();
            this.rbUSD = new MetroFramework.Controls.MetroRadioButton();
            this.rbOther = new MetroFramework.Controls.MetroRadioButton();
            this.rbGBP = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonCalculate = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.textBoxItem = new MetroFramework.Controls.MetroTextBox();
            this.textBoxKeys = new MetroFramework.Controls.MetroTextBox();
            this.Percentage = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.textBoxKeysOut = new MetroFramework.Controls.MetroTextBox();
            this.textBoxItemOut = new MetroFramework.Controls.MetroTextBox();
            this.textBoxMarketItem = new MetroFramework.Controls.MetroTextBox();
            this.textBoxMarketKeys = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonMarket = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCSGOAnalyst = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMetjm = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZone = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExchange = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLounge = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonReddit = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBot = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRep = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStash = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOPSkins = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBitSkins = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSIH = new System.Windows.Forms.ToolStripButton();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.Main = new MetroFramework.Controls.MetroTabPage();
            this.Float = new MetroFramework.Controls.MetroTabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanelInventory = new System.Windows.Forms.FlowLayoutPanel();
            this.metroPanel5 = new MetroFramework.Controls.MetroPanel();
            this.labelError = new MetroFramework.Controls.MetroLabel();
            this.textBoxSteamID = new MetroFramework.Controls.MetroTextBox();
            this.Avatar = new System.Windows.Forms.PictureBox();
            this.buttonCheckFloat = new MetroFramework.Controls.MetroButton();
            this.Cashout = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel6 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxPP = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxREC = new MetroFramework.Controls.MetroTextBox();
            this.metroTogglePremium = new MetroFramework.Controls.MetroToggle();
            this.metroTextBoxOP = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.Settings = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxColor = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroToggleNightMode = new MetroFramework.Controls.MetroToggle();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroToggleScreenLock = new MetroFramework.Controls.MetroToggle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.metroPanel2.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.metroPanel4.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.metroTabControl1.SuspendLayout();
            this.Main.SuspendLayout();
            this.Float.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).BeginInit();
            this.Cashout.SuspendLayout();
            this.metroPanel6.SuspendLayout();
            this.Settings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.tableLayoutPanel15);
            this.metroPanel2.Controls.Add(this.metroLabel9);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(3, 3);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(188, 132);
            this.metroPanel2.TabIndex = 0;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.buttonSwitch, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.tableLayoutPanel17, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.tableLayoutPanel18, 0, 2);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 3;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(188, 107);
            this.tableLayoutPanel15.TabIndex = 3;
            // 
            // buttonSwitch
            // 
            this.buttonSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSwitch.Location = new System.Drawing.Point(3, 39);
            this.buttonSwitch.Name = "buttonSwitch";
            this.buttonSwitch.Size = new System.Drawing.Size(182, 29);
            this.buttonSwitch.TabIndex = 0;
            this.buttonSwitch.Text = "Switch";
            this.buttonSwitch.Click += new System.EventHandler(this.buttonSwitch_Click);
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 4;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Controls.Add(this.metroLabel11, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.metroLabel12, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.comboBoxFrom, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.comboBoxTo, 3, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(182, 30);
            this.tableLayoutPanel17.TabIndex = 1;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel11.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel11.Location = new System.Drawing.Point(3, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(37, 30);
            this.metroLabel11.TabIndex = 0;
            this.metroLabel11.Text = "From:";
            this.metroLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel12.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel12.Location = new System.Drawing.Point(102, 0);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(21, 30);
            this.metroLabel12.TabIndex = 1;
            this.metroLabel12.Text = "To:";
            this.metroLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxFrom
            // 
            this.comboBoxFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxFrom.FontSize = MetroFramework.MetroLinkSize.Small;
            this.comboBoxFrom.FormattingEnabled = true;
            this.comboBoxFrom.ItemHeight = 19;
            this.comboBoxFrom.Items.AddRange(new object[] {
            "EUR"});
            this.comboBoxFrom.Location = new System.Drawing.Point(46, 3);
            this.comboBoxFrom.Name = "comboBoxFrom";
            this.comboBoxFrom.Size = new System.Drawing.Size(50, 25);
            this.comboBoxFrom.TabIndex = 2;
            // 
            // comboBoxTo
            // 
            this.comboBoxTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxTo.FontSize = MetroFramework.MetroLinkSize.Small;
            this.comboBoxTo.FormattingEnabled = true;
            this.comboBoxTo.ItemHeight = 19;
            this.comboBoxTo.Location = new System.Drawing.Point(129, 3);
            this.comboBoxTo.Name = "comboBoxTo";
            this.comboBoxTo.Size = new System.Drawing.Size(50, 25);
            this.comboBoxTo.TabIndex = 3;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.textBoxFrom, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.textBoxTo, 1, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 74);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(182, 30);
            this.tableLayoutPanel18.TabIndex = 2;
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFrom.Location = new System.Drawing.Point(3, 3);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.Size = new System.Drawing.Size(85, 24);
            this.textBoxFrom.TabIndex = 0;
            this.textBoxFrom.Text = "1";
            this.textBoxFrom.TextChanged += new System.EventHandler(this.textBoxFrom_TextChanged);
            // 
            // textBoxTo
            // 
            this.textBoxTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTo.Location = new System.Drawing.Point(94, 3);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(85, 24);
            this.textBoxTo.TabIndex = 1;
            this.textBoxTo.Text = "1";
            this.textBoxTo.TextChanged += new System.EventHandler(this.textBoxTo_TextChanged);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel9.Location = new System.Drawing.Point(0, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(178, 25);
            this.metroLabel9.TabIndex = 2;
            this.metroLabel9.Text = "Currency Converter";
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.tableLayoutPanel16);
            this.metroPanel3.Controls.Add(this.metroLabel10);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(197, 3);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(116, 132);
            this.metroPanel3.TabIndex = 1;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.metroPanel4, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel19, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(116, 107);
            this.tableLayoutPanel16.TabIndex = 1;
            // 
            // metroPanel4
            // 
            this.metroPanel4.Controls.Add(this.labelEnterKeyPrice);
            this.metroPanel4.Controls.Add(this.textBoxKeyVal);
            this.metroPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel4.HorizontalScrollbarBarColor = true;
            this.metroPanel4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel4.HorizontalScrollbarSize = 10;
            this.metroPanel4.Location = new System.Drawing.Point(3, 56);
            this.metroPanel4.Name = "metroPanel4";
            this.metroPanel4.Size = new System.Drawing.Size(110, 48);
            this.metroPanel4.TabIndex = 0;
            this.metroPanel4.VerticalScrollbarBarColor = true;
            this.metroPanel4.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel4.VerticalScrollbarSize = 10;
            // 
            // labelEnterKeyPrice
            // 
            this.labelEnterKeyPrice.AutoSize = true;
            this.labelEnterKeyPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEnterKeyPrice.Enabled = false;
            this.labelEnterKeyPrice.FontSize = MetroFramework.MetroLabelSize.Small;
            this.labelEnterKeyPrice.Location = new System.Drawing.Point(0, 23);
            this.labelEnterKeyPrice.Name = "labelEnterKeyPrice";
            this.labelEnterKeyPrice.Size = new System.Drawing.Size(54, 15);
            this.labelEnterKeyPrice.TabIndex = 3;
            this.labelEnterKeyPrice.Text = "Key price.";
            // 
            // textBoxKeyVal
            // 
            this.textBoxKeyVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxKeyVal.Enabled = false;
            this.textBoxKeyVal.Location = new System.Drawing.Point(0, 0);
            this.textBoxKeyVal.Name = "textBoxKeyVal";
            this.textBoxKeyVal.Size = new System.Drawing.Size(110, 23);
            this.textBoxKeyVal.TabIndex = 2;
            this.textBoxKeyVal.Text = "160";
            this.textBoxKeyVal.TextChanged += new System.EventHandler(this.textBoxKeyVal_TextChanged);
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.rbEUR, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.rbUSD, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.rbOther, 1, 1);
            this.tableLayoutPanel19.Controls.Add(this.rbGBP, 0, 1);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(110, 47);
            this.tableLayoutPanel19.TabIndex = 1;
            // 
            // rbEUR
            // 
            this.rbEUR.AutoSize = true;
            this.rbEUR.Checked = true;
            this.rbEUR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbEUR.Location = new System.Drawing.Point(3, 3);
            this.rbEUR.Name = "rbEUR";
            this.rbEUR.Size = new System.Drawing.Size(49, 17);
            this.rbEUR.TabIndex = 0;
            this.rbEUR.TabStop = true;
            this.rbEUR.Text = "EUR";
            this.rbEUR.UseVisualStyleBackColor = true;
            this.rbEUR.CheckedChanged += new System.EventHandler(this.rbEUR_CheckedChanged);
            // 
            // rbUSD
            // 
            this.rbUSD.AutoSize = true;
            this.rbUSD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbUSD.Location = new System.Drawing.Point(58, 3);
            this.rbUSD.Name = "rbUSD";
            this.rbUSD.Size = new System.Drawing.Size(49, 17);
            this.rbUSD.TabIndex = 1;
            this.rbUSD.Text = "USD";
            this.rbUSD.UseVisualStyleBackColor = true;
            this.rbUSD.CheckedChanged += new System.EventHandler(this.rbUSD_CheckedChanged);
            // 
            // rbOther
            // 
            this.rbOther.AutoSize = true;
            this.rbOther.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbOther.Location = new System.Drawing.Point(58, 26);
            this.rbOther.Name = "rbOther";
            this.rbOther.Size = new System.Drawing.Size(49, 18);
            this.rbOther.TabIndex = 2;
            this.rbOther.Text = "Other";
            this.rbOther.UseVisualStyleBackColor = true;
            this.rbOther.CheckedChanged += new System.EventHandler(this.rbOther_CheckedChanged);
            // 
            // rbGBP
            // 
            this.rbGBP.AutoSize = true;
            this.rbGBP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbGBP.Location = new System.Drawing.Point(3, 26);
            this.rbGBP.Name = "rbGBP";
            this.rbGBP.Size = new System.Drawing.Size(49, 18);
            this.rbGBP.TabIndex = 3;
            this.rbGBP.Text = "GBP";
            this.rbGBP.UseVisualStyleBackColor = true;
            this.rbGBP.CheckedChanged += new System.EventHandler(this.rbGBP_CheckedChanged);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel10.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel10.Location = new System.Drawing.Point(0, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(91, 25);
            this.metroLabel10.TabIndex = 0;
            this.metroLabel10.Text = "Key Price";
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.tableLayoutPanel12);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(316, 275);
            this.metroPanel1.TabIndex = 30;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.buttonCalculate, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel14, 0, 2);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 3;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.85F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57.15F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(316, 250);
            this.tableLayoutPanel12.TabIndex = 3;
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCalculate.Location = new System.Drawing.Point(3, 95);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(310, 29);
            this.buttonCalculate.TabIndex = 0;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.metroLabel2, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.metroLabel3, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.metroLabel4, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.textBoxItem, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.textBoxKeys, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.Percentage, 1, 2);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 3;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(310, 86);
            this.tableLayoutPanel13.TabIndex = 1;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel2.Location = new System.Drawing.Point(3, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(274, 28);
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "Price of the item.";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel3.Location = new System.Drawing.Point(3, 28);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(274, 28);
            this.metroLabel3.TabIndex = 1;
            this.metroLabel3.Text = "Price of the item in keys.";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel4.Location = new System.Drawing.Point(3, 56);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(274, 30);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Percentage.";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxItem
            // 
            this.textBoxItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxItem.Location = new System.Drawing.Point(283, 3);
            this.textBoxItem.Name = "textBoxItem";
            this.textBoxItem.Size = new System.Drawing.Size(24, 22);
            this.textBoxItem.TabIndex = 3;
            this.textBoxItem.Text = "0";
            this.textBoxItem.TextChanged += new System.EventHandler(this.textBoxItem_TextChanged);
            // 
            // textBoxKeys
            // 
            this.textBoxKeys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxKeys.Location = new System.Drawing.Point(283, 31);
            this.textBoxKeys.Name = "textBoxKeys";
            this.textBoxKeys.Size = new System.Drawing.Size(24, 22);
            this.textBoxKeys.TabIndex = 4;
            this.textBoxKeys.Text = "0";
            this.textBoxKeys.TextChanged += new System.EventHandler(this.textBoxKeys_TextChanged);
            // 
            // Percentage
            // 
            this.Percentage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Percentage.Location = new System.Drawing.Point(283, 59);
            this.Percentage.Name = "Percentage";
            this.Percentage.Size = new System.Drawing.Size(24, 24);
            this.Percentage.TabIndex = 5;
            this.Percentage.Text = "85";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.metroLabel5, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.metroLabel7, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.metroLabel8, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.textBoxKeysOut, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.textBoxItemOut, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.textBoxMarketItem, 1, 2);
            this.tableLayoutPanel14.Controls.Add(this.textBoxMarketKeys, 1, 3);
            this.tableLayoutPanel14.Controls.Add(this.metroLabel6, 0, 1);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 130);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 4;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(310, 117);
            this.tableLayoutPanel14.TabIndex = 2;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel5.Location = new System.Drawing.Point(3, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(274, 29);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Price of the item with % added to it.";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel7.Location = new System.Drawing.Point(3, 58);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(274, 29);
            this.metroLabel7.TabIndex = 2;
            this.metroLabel7.Text = "Ammount you receive when selling on market.";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel8.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel8.Location = new System.Drawing.Point(3, 87);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(274, 30);
            this.metroLabel8.TabIndex = 3;
            this.metroLabel8.Text = "Ammount you receive when selling on market in keys.";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxKeysOut
            // 
            this.textBoxKeysOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxKeysOut.Location = new System.Drawing.Point(283, 3);
            this.textBoxKeysOut.Name = "textBoxKeysOut";
            this.textBoxKeysOut.Size = new System.Drawing.Size(24, 23);
            this.textBoxKeysOut.TabIndex = 4;
            this.textBoxKeysOut.Text = "0";
            // 
            // textBoxItemOut
            // 
            this.textBoxItemOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxItemOut.Location = new System.Drawing.Point(283, 32);
            this.textBoxItemOut.Name = "textBoxItemOut";
            this.textBoxItemOut.Size = new System.Drawing.Size(24, 23);
            this.textBoxItemOut.TabIndex = 5;
            this.textBoxItemOut.Text = "0";
            // 
            // textBoxMarketItem
            // 
            this.textBoxMarketItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMarketItem.Location = new System.Drawing.Point(283, 61);
            this.textBoxMarketItem.Name = "textBoxMarketItem";
            this.textBoxMarketItem.Size = new System.Drawing.Size(24, 23);
            this.textBoxMarketItem.TabIndex = 6;
            this.textBoxMarketItem.Text = "0";
            // 
            // textBoxMarketKeys
            // 
            this.textBoxMarketKeys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMarketKeys.Location = new System.Drawing.Point(283, 90);
            this.textBoxMarketKeys.Name = "textBoxMarketKeys";
            this.textBoxMarketKeys.Size = new System.Drawing.Size(24, 24);
            this.textBoxMarketKeys.TabIndex = 7;
            this.textBoxMarketKeys.Text = "0";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel6.Location = new System.Drawing.Point(3, 29);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(274, 29);
            this.metroLabel6.TabIndex = 8;
            this.metroLabel6.Text = "Price of the item in keys with %.";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(179, 25);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Quicksell Converter";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.toolStripButtonMarket,
            this.toolStripButtonCSGOAnalyst,
            this.toolStripButtonMetjm,
            this.toolStripButtonZone,
            this.toolStripButtonExchange,
            this.toolStripButtonLounge,
            this.toolStripButtonReddit,
            this.toolStripButtonBot,
            this.toolStripButtonRep,
            this.toolStripButtonStash,
            this.toolStripButtonOPSkins,
            this.toolStripButtonBitSkins,
            this.toolStripButtonSIH});
            this.toolStrip1.Location = new System.Drawing.Point(340, 30);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(90, 464);
            this.toolStrip1.TabIndex = 29;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(87, 15);
            this.toolStripLabel1.Text = "Useful Sites";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(87, 6);
            // 
            // toolStripButtonMarket
            // 
            this.toolStripButtonMarket.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMarket.Image")));
            this.toolStripButtonMarket.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMarket.Name = "toolStripButtonMarket";
            this.toolStripButtonMarket.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonMarket.Text = "Market";
            this.toolStripButtonMarket.Click += new System.EventHandler(this.toolStripButtonMarket_Click);
            // 
            // toolStripButtonCSGOAnalyst
            // 
            this.toolStripButtonCSGOAnalyst.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCSGOAnalyst.Image")));
            this.toolStripButtonCSGOAnalyst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCSGOAnalyst.Name = "toolStripButtonCSGOAnalyst";
            this.toolStripButtonCSGOAnalyst.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonCSGOAnalyst.Text = "Analyst";
            this.toolStripButtonCSGOAnalyst.Click += new System.EventHandler(this.toolStripButtonCSGOAnalyst_Click);
            // 
            // toolStripButtonMetjm
            // 
            this.toolStripButtonMetjm.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMetjm.Image")));
            this.toolStripButtonMetjm.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMetjm.Name = "toolStripButtonMetjm";
            this.toolStripButtonMetjm.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonMetjm.Text = "Metjm";
            this.toolStripButtonMetjm.Click += new System.EventHandler(this.toolStripButtonMetjm_Click);
            // 
            // toolStripButtonZone
            // 
            this.toolStripButtonZone.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZone.Image")));
            this.toolStripButtonZone.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZone.Name = "toolStripButtonZone";
            this.toolStripButtonZone.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonZone.Text = "Zone";
            this.toolStripButtonZone.Click += new System.EventHandler(this.toolStripButtonZone_Click);
            // 
            // toolStripButtonExchange
            // 
            this.toolStripButtonExchange.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonExchange.Image")));
            this.toolStripButtonExchange.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExchange.Name = "toolStripButtonExchange";
            this.toolStripButtonExchange.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonExchange.Text = "Exchange";
            this.toolStripButtonExchange.Click += new System.EventHandler(this.toolStripButtonExchange_Click);
            // 
            // toolStripButtonLounge
            // 
            this.toolStripButtonLounge.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLounge.Image")));
            this.toolStripButtonLounge.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLounge.Name = "toolStripButtonLounge";
            this.toolStripButtonLounge.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonLounge.Text = "Lounge";
            this.toolStripButtonLounge.Click += new System.EventHandler(this.toolStripButtonLounge_Click);
            // 
            // toolStripButtonReddit
            // 
            this.toolStripButtonReddit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonReddit.Image")));
            this.toolStripButtonReddit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReddit.Name = "toolStripButtonReddit";
            this.toolStripButtonReddit.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonReddit.Text = "Reddit";
            this.toolStripButtonReddit.Click += new System.EventHandler(this.toolStripButtonReddit_Click);
            // 
            // toolStripButtonBot
            // 
            this.toolStripButtonBot.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBot.Image")));
            this.toolStripButtonBot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBot.Name = "toolStripButtonBot";
            this.toolStripButtonBot.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonBot.Text = "[H]/[W] Bot";
            this.toolStripButtonBot.Click += new System.EventHandler(this.toolStripButtonBot_Click);
            // 
            // toolStripButtonRep
            // 
            this.toolStripButtonRep.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRep.Image")));
            this.toolStripButtonRep.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRep.Name = "toolStripButtonRep";
            this.toolStripButtonRep.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonRep.Text = "Steam REP";
            this.toolStripButtonRep.Click += new System.EventHandler(this.toolStripButtonRep_Click);
            // 
            // toolStripButtonStash
            // 
            this.toolStripButtonStash.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStash.Image")));
            this.toolStripButtonStash.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStash.Name = "toolStripButtonStash";
            this.toolStripButtonStash.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonStash.Text = "Stash";
            this.toolStripButtonStash.Click += new System.EventHandler(this.toolStripButtonStash_Click);
            // 
            // toolStripButtonOPSkins
            // 
            this.toolStripButtonOPSkins.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOPSkins.Image")));
            this.toolStripButtonOPSkins.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOPSkins.Name = "toolStripButtonOPSkins";
            this.toolStripButtonOPSkins.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonOPSkins.Text = "OPSkins";
            this.toolStripButtonOPSkins.Click += new System.EventHandler(this.toolStripButtonOPSkins_Click);
            // 
            // toolStripButtonBitSkins
            // 
            this.toolStripButtonBitSkins.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBitSkins.Image")));
            this.toolStripButtonBitSkins.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBitSkins.Name = "toolStripButtonBitSkins";
            this.toolStripButtonBitSkins.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonBitSkins.Text = "BitSkins";
            this.toolStripButtonBitSkins.Click += new System.EventHandler(this.toolStripButtonBitSkins_Click);
            // 
            // toolStripButtonSIH
            // 
            this.toolStripButtonSIH.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSIH.Image")));
            this.toolStripButtonSIH.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSIH.Name = "toolStripButtonSIH";
            this.toolStripButtonSIH.Size = new System.Drawing.Size(87, 20);
            this.toolStripButtonSIH.Text = "S.I.H.";
            this.toolStripButtonSIH.Click += new System.EventHandler(this.toolStripButtonSIH_Click);
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            this.metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroStyleManager1.MetroStyleChanged += new System.EventHandler(this.styleChanged);
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel14.Location = new System.Drawing.Point(10, 8);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(71, 19);
            this.metroLabel14.TabIndex = 30;
            this.metroLabel14.Text = "TradeBro";
            this.metroLabel14.UseStyleColors = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel11, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(322, 425);
            this.tableLayoutPanel10.TabIndex = 28;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.53846F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.46154F));
            this.tableLayoutPanel11.Controls.Add(this.metroPanel2, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.metroPanel3, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 284);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(316, 138);
            this.tableLayoutPanel11.TabIndex = 27;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.Main);
            this.metroTabControl1.Controls.Add(this.Float);
            this.metroTabControl1.Controls.Add(this.Cashout);
            this.metroTabControl1.Controls.Add(this.Settings);
            this.metroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl1.Location = new System.Drawing.Point(10, 30);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(330, 464);
            this.metroTabControl1.TabIndex = 0;
            // 
            // Main
            // 
            this.Main.Controls.Add(this.tableLayoutPanel10);
            this.Main.HorizontalScrollbarBarColor = true;
            this.Main.HorizontalScrollbarSize = 0;
            this.Main.Location = new System.Drawing.Point(4, 35);
            this.Main.Name = "Main";
            this.Main.Size = new System.Drawing.Size(322, 425);
            this.Main.TabIndex = 0;
            this.Main.Text = "Main";
            this.Main.VerticalScrollbarBarColor = true;
            this.Main.VerticalScrollbarSize = 0;
            // 
            // Float
            // 
            this.Float.Controls.Add(this.tableLayoutPanel1);
            this.Float.HorizontalScrollbarBarColor = true;
            this.Float.HorizontalScrollbarSize = 0;
            this.Float.Location = new System.Drawing.Point(4, 35);
            this.Float.Name = "Float";
            this.Float.Size = new System.Drawing.Size(322, 425);
            this.Float.TabIndex = 1;
            this.Float.Text = "Float";
            this.Float.VerticalScrollbarBarColor = true;
            this.Float.VerticalScrollbarSize = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanelInventory, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel5, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(322, 425);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // flowLayoutPanelInventory
            // 
            this.flowLayoutPanelInventory.AutoScroll = true;
            this.flowLayoutPanelInventory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelInventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelInventory.Location = new System.Drawing.Point(3, 103);
            this.flowLayoutPanelInventory.Name = "flowLayoutPanelInventory";
            this.flowLayoutPanelInventory.Size = new System.Drawing.Size(316, 319);
            this.flowLayoutPanelInventory.TabIndex = 7;
            // 
            // metroPanel5
            // 
            this.metroPanel5.Controls.Add(this.labelError);
            this.metroPanel5.Controls.Add(this.textBoxSteamID);
            this.metroPanel5.Controls.Add(this.Avatar);
            this.metroPanel5.Controls.Add(this.buttonCheckFloat);
            this.metroPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel5.HorizontalScrollbarBarColor = true;
            this.metroPanel5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel5.HorizontalScrollbarSize = 10;
            this.metroPanel5.Location = new System.Drawing.Point(3, 3);
            this.metroPanel5.Name = "metroPanel5";
            this.metroPanel5.Size = new System.Drawing.Size(316, 94);
            this.metroPanel5.TabIndex = 8;
            this.metroPanel5.VerticalScrollbarBarColor = true;
            this.metroPanel5.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel5.VerticalScrollbarSize = 10;
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Location = new System.Drawing.Point(3, 58);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(210, 19);
            this.labelError.TabIndex = 7;
            this.labelError.Text = "Steam API servers are unavailable.";
            this.labelError.Visible = false;
            // 
            // textBoxSteamID
            // 
            this.textBoxSteamID.Location = new System.Drawing.Point(3, 3);
            this.textBoxSteamID.Name = "textBoxSteamID";
            this.textBoxSteamID.Size = new System.Drawing.Size(210, 23);
            this.textBoxSteamID.TabIndex = 4;
            this.textBoxSteamID.Text = "Steam ID";
            // 
            // Avatar
            // 
            this.Avatar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Avatar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Avatar.Location = new System.Drawing.Point(222, 0);
            this.Avatar.Name = "Avatar";
            this.Avatar.Size = new System.Drawing.Size(94, 94);
            this.Avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Avatar.TabIndex = 6;
            this.Avatar.TabStop = false;
            // 
            // buttonCheckFloat
            // 
            this.buttonCheckFloat.Location = new System.Drawing.Point(3, 32);
            this.buttonCheckFloat.Name = "buttonCheckFloat";
            this.buttonCheckFloat.Size = new System.Drawing.Size(210, 23);
            this.buttonCheckFloat.TabIndex = 2;
            this.buttonCheckFloat.Text = "Check Float";
            this.buttonCheckFloat.Click += new System.EventHandler(this.buttonCheckFloat_Click);
            // 
            // Cashout
            // 
            this.Cashout.Controls.Add(this.metroPanel6);
            this.Cashout.HorizontalScrollbarBarColor = true;
            this.Cashout.HorizontalScrollbarSize = 0;
            this.Cashout.Location = new System.Drawing.Point(4, 35);
            this.Cashout.Name = "Cashout";
            this.Cashout.Size = new System.Drawing.Size(322, 425);
            this.Cashout.TabIndex = 4;
            this.Cashout.Text = "Cashout";
            this.Cashout.VerticalScrollbarBarColor = true;
            this.Cashout.VerticalScrollbarSize = 0;
            // 
            // metroPanel6
            // 
            this.metroPanel6.Controls.Add(this.metroLabel24);
            this.metroPanel6.Controls.Add(this.metroLabel23);
            this.metroPanel6.Controls.Add(this.metroTextBoxPP);
            this.metroPanel6.Controls.Add(this.metroTextBoxREC);
            this.metroPanel6.Controls.Add(this.metroTogglePremium);
            this.metroPanel6.Controls.Add(this.metroTextBoxOP);
            this.metroPanel6.Controls.Add(this.metroLabel19);
            this.metroPanel6.Controls.Add(this.metroLabel20);
            this.metroPanel6.Controls.Add(this.metroLabel21);
            this.metroPanel6.Controls.Add(this.metroLabel22);
            this.metroPanel6.HorizontalScrollbarBarColor = true;
            this.metroPanel6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel6.HorizontalScrollbarSize = 10;
            this.metroPanel6.Location = new System.Drawing.Point(3, 3);
            this.metroPanel6.Name = "metroPanel6";
            this.metroPanel6.Size = new System.Drawing.Size(319, 171);
            this.metroPanel6.TabIndex = 11;
            this.metroPanel6.VerticalScrollbarBarColor = true;
            this.metroPanel6.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel6.VerticalScrollbarSize = 10;
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.Location = new System.Drawing.Point(95, 72);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(121, 38);
            this.metroLabel24.TabIndex = 11;
            this.metroLabel24.Text = " |                        |\r\n▼                      ▼";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel23.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel23.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel23.Location = new System.Drawing.Point(0, 0);
            this.metroLabel23.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(81, 25);
            this.metroLabel23.TabIndex = 10;
            this.metroLabel23.Text = "Cashout";
            // 
            // metroTextBoxPP
            // 
            this.metroTextBoxPP.Location = new System.Drawing.Point(167, 138);
            this.metroTextBoxPP.Name = "metroTextBoxPP";
            this.metroTextBoxPP.Size = new System.Drawing.Size(149, 23);
            this.metroTextBoxPP.TabIndex = 4;
            this.metroTextBoxPP.Text = "0";
            // 
            // metroTextBoxREC
            // 
            this.metroTextBoxREC.Location = new System.Drawing.Point(167, 112);
            this.metroTextBoxREC.Name = "metroTextBoxREC";
            this.metroTextBoxREC.Size = new System.Drawing.Size(149, 23);
            this.metroTextBoxREC.TabIndex = 3;
            this.metroTextBoxREC.Text = "0";
            // 
            // metroTogglePremium
            // 
            this.metroTogglePremium.AutoSize = true;
            this.metroTogglePremium.Location = new System.Drawing.Point(167, 50);
            this.metroTogglePremium.Name = "metroTogglePremium";
            this.metroTogglePremium.Size = new System.Drawing.Size(80, 17);
            this.metroTogglePremium.TabIndex = 5;
            this.metroTogglePremium.Text = "Off";
            this.metroTogglePremium.UseVisualStyleBackColor = true;
            this.metroTogglePremium.CheckedChanged += new System.EventHandler(this.metroTogglePremium_CheckedChanged);
            // 
            // metroTextBoxOP
            // 
            this.metroTextBoxOP.Location = new System.Drawing.Point(167, 21);
            this.metroTextBoxOP.Name = "metroTextBoxOP";
            this.metroTextBoxOP.Size = new System.Drawing.Size(149, 23);
            this.metroTextBoxOP.TabIndex = 2;
            this.metroTextBoxOP.Text = "0";
            this.metroTextBoxOP.TextChanged += new System.EventHandler(this.metroTextBoxOB_TextChanged);
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel19.Location = new System.Drawing.Point(3, 25);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(150, 15);
            this.metroLabel19.TabIndex = 6;
            this.metroLabel19.Text = "Price of the item on OPSkins.";
            this.metroLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel20.Location = new System.Drawing.Point(3, 52);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(55, 15);
            this.metroLabel20.TabIndex = 7;
            this.metroLabel20.Text = "Premium.";
            this.metroLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel21.Location = new System.Drawing.Point(3, 120);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(168, 15);
            this.metroLabel21.TabIndex = 8;
            this.metroLabel21.Text = "Amount you receive on OPSkins.";
            this.metroLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel22.Location = new System.Drawing.Point(3, 146);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(159, 15);
            this.metroLabel22.TabIndex = 9;
            this.metroLabel22.Text = "Amount you receive on PayPal.";
            this.metroLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Settings
            // 
            this.Settings.Controls.Add(this.metroLabel16);
            this.Settings.Controls.Add(this.metroComboBoxColor);
            this.Settings.Controls.Add(this.metroLabel15);
            this.Settings.Controls.Add(this.metroToggleNightMode);
            this.Settings.Controls.Add(this.metroLabel13);
            this.Settings.Controls.Add(this.metroToggleScreenLock);
            this.Settings.HorizontalScrollbarBarColor = true;
            this.Settings.Location = new System.Drawing.Point(4, 35);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(322, 425);
            this.Settings.TabIndex = 2;
            this.Settings.Text = "Settings";
            this.Settings.VerticalScrollbarBarColor = true;
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(3, 73);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(42, 19);
            this.metroLabel16.TabIndex = 7;
            this.metroLabel16.Text = "Color";
            // 
            // metroComboBoxColor
            // 
            this.metroComboBoxColor.FormattingEnabled = true;
            this.metroComboBoxColor.ItemHeight = 23;
            this.metroComboBoxColor.Items.AddRange(new object[] {
            "Black",
            "White",
            "Gray",
            "Default",
            "Green",
            "Lime",
            "Blue",
            "Orange",
            "Brown",
            "Pink",
            "Magenta",
            "Purple",
            "Red",
            "Yellow"});
            this.metroComboBoxColor.Location = new System.Drawing.Point(87, 63);
            this.metroComboBoxColor.MaxLength = 9;
            this.metroComboBoxColor.Name = "metroComboBoxColor";
            this.metroComboBoxColor.Size = new System.Drawing.Size(121, 29);
            this.metroComboBoxColor.TabIndex = 6;
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(3, 38);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(80, 19);
            this.metroLabel15.TabIndex = 5;
            this.metroLabel15.Text = "Night Mode";
            // 
            // metroToggleNightMode
            // 
            this.metroToggleNightMode.AutoSize = true;
            this.metroToggleNightMode.Location = new System.Drawing.Point(87, 40);
            this.metroToggleNightMode.Name = "metroToggleNightMode";
            this.metroToggleNightMode.Size = new System.Drawing.Size(80, 17);
            this.metroToggleNightMode.TabIndex = 4;
            this.metroToggleNightMode.Text = "Off";
            this.metroToggleNightMode.UseVisualStyleBackColor = true;
            this.metroToggleNightMode.CheckedChanged += new System.EventHandler(this.metroToggleNightMode_CheckedChanged);
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(3, 15);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(78, 19);
            this.metroLabel13.TabIndex = 3;
            this.metroLabel13.Text = "Screen Lock";
            // 
            // metroToggleScreenLock
            // 
            this.metroToggleScreenLock.AutoSize = true;
            this.metroToggleScreenLock.Checked = true;
            this.metroToggleScreenLock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroToggleScreenLock.Location = new System.Drawing.Point(87, 17);
            this.metroToggleScreenLock.Name = "metroToggleScreenLock";
            this.metroToggleScreenLock.Size = new System.Drawing.Size(80, 17);
            this.metroToggleScreenLock.TabIndex = 2;
            this.metroToggleScreenLock.Text = "On";
            this.metroToggleScreenLock.UseVisualStyleBackColor = true;
            this.metroToggleScreenLock.CheckedChanged += new System.EventHandler(this.metroToggleScreenLock_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(286, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 32;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(440, 504);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.metroTabControl1);
            this.Controls.Add(this.toolStrip1);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(440, 504);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(10, 30, 10, 10);
            this.Text = "TradeBro";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.metroPanel4.ResumeLayout(false);
            this.metroPanel4.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.metroTabControl1.ResumeLayout(false);
            this.Main.ResumeLayout(false);
            this.Float.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.metroPanel5.ResumeLayout(false);
            this.metroPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).EndInit();
            this.Cashout.ResumeLayout(false);
            this.metroPanel6.ResumeLayout(false);
            this.metroPanel6.PerformLayout();
            this.Settings.ResumeLayout(false);
            this.Settings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonCSGOAnalyst;
        private System.Windows.Forms.ToolStripButton toolStripButtonMetjm;
        private System.Windows.Forms.ToolStripButton toolStripButtonZone;
        private System.Windows.Forms.ToolStripButton toolStripButtonExchange;
        private System.Windows.Forms.ToolStripButton toolStripButtonLounge;
        private System.Windows.Forms.ToolStripButton toolStripButtonReddit;
        private System.Windows.Forms.ToolStripButton toolStripButtonBot;
        private System.Windows.Forms.ToolStripButton toolStripButtonSIH;
        private System.Windows.Forms.ToolStripButton toolStripButtonMarket;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton buttonCalculate;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox textBoxItem;
        private MetroFramework.Controls.MetroTextBox textBoxKeys;
        private MetroFramework.Controls.MetroTextBox Percentage;
        private MetroFramework.Controls.MetroTextBox textBoxKeysOut;
        private MetroFramework.Controls.MetroTextBox textBoxItemOut;
        private MetroFramework.Controls.MetroTextBox textBoxMarketItem;
        private MetroFramework.Controls.MetroTextBox textBoxMarketKeys;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private MetroFramework.Controls.MetroButton buttonSwitch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroComboBox comboBoxFrom;
        private MetroFramework.Controls.MetroComboBox comboBoxTo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private MetroFramework.Controls.MetroTextBox textBoxFrom;
        private MetroFramework.Controls.MetroTextBox textBoxTo;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private MetroFramework.Controls.MetroPanel metroPanel4;
        private MetroFramework.Controls.MetroLabel labelEnterKeyPrice;
        private MetroFramework.Controls.MetroTextBox textBoxKeyVal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private MetroFramework.Controls.MetroRadioButton rbEUR;
        private MetroFramework.Controls.MetroRadioButton rbUSD;
        private MetroFramework.Controls.MetroRadioButton rbOther;
        private MetroFramework.Controls.MetroRadioButton rbGBP;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage Main;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private MetroFramework.Controls.MetroTabPage Float;
        private MetroFramework.Controls.MetroTabPage Settings;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroToggle metroToggleNightMode;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroToggle metroToggleScreenLock;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroComboBox metroComboBoxColor;
        private System.Windows.Forms.ToolStripButton toolStripButtonStash;
        private System.Windows.Forms.ToolStripButton toolStripButtonRep;
        private System.Windows.Forms.ToolStripButton toolStripButtonBitSkins;
        private System.Windows.Forms.ToolStripButton toolStripButtonOPSkins;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTextBox textBoxSteamID;
        private MetroFramework.Controls.MetroButton buttonCheckFloat;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelInventory;
        private System.Windows.Forms.PictureBox Avatar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel5;
        private MetroFramework.Controls.MetroTabPage Cashout;
        private System.Windows.Forms.ToolTip toolTip1;
        private MetroFramework.Controls.MetroLabel labelError;
        private MetroFramework.Controls.MetroTextBox metroTextBoxPP;
        private MetroFramework.Controls.MetroTextBox metroTextBoxREC;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOP;
        private MetroFramework.Controls.MetroToggle metroTogglePremium;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private MetroFramework.Controls.MetroPanel metroPanel6;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private MetroFramework.Controls.MetroLabel metroLabel6;
    }
}

