﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSGOTraderSystem
{
    partial class Form1
    {
        private void metroTextBoxOB_TextChanged(object sender, EventArgs e)
        {
            calculateCashout();
        }

        private void metroTogglePremium_CheckedChanged(object sender, EventArgs e)
        {
            calculateCashout();
        }

        //Codded by MandIch and improved by OOrosh
        private void calculateCashout()
        {
            double x = 0, uc, ppc;
            if (double.TryParse(
                (metroTextBoxOP.Text == "") ? "0" : metroTextBoxOP.Text, 
                out x))
            {
                uc = x - x * ((metroTogglePremium.Checked == true) ? 0.05 : 0.10);
                ppc = uc - uc * 0.029;
                metroTextBoxREC.Text = Convert.ToString(Math.Round(uc, 2));
                metroTextBoxPP.Text = Convert.ToString(Math.Round(ppc, 2));
            }

        }
    }
}
