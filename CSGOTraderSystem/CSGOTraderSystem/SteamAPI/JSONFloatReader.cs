﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;
using System.Net;

namespace CSGOTraderSystem
{
    class JSONFloatReader
    {
        public JSONFloatReader()
        {

        }

        public double ReadFloat(Int64 steamid, Int64 assetid)
        {
            string file = new WebClient().DownloadString("https://api.steampowered.com/IEconItems_730/GetPlayerItems/v0001/?key=DCD148674953187B729DEBCC07497510&SteamID=" + steamid.ToString());
            JavaScriptSerializer js = new JavaScriptSerializer();
            Inentory r = js.Deserialize<Inentory>(file);
            foreach (Item i in r.result.items)
            {
                if (i.id == assetid)
                {
                    if (i.attributes != null)
                    {
                        foreach (Atribute a in i.attributes)
                        {
                            if (a.defindex == 8)
                            {
                                return a.float_value;
                            }
                        }
                    }
                }
            }
            //Error
            return -1;
        }

        public Item[] ReadJson(Int64 steamid)
        {
            string file;
            try
            {
                file = new WebClient().DownloadString("https://api.steampowered.com/IEconItems_730/GetPlayerItems/v0001/?key=DCD148674953187B729DEBCC07497510&SteamID=" + steamid.ToString());
            }
            catch (Exception)
            {
                return null;
            }
            
            JavaScriptSerializer js = new JavaScriptSerializer();
            Inentory r = js.Deserialize<Inentory>(file);

            if (r.result.status == 1)
            {
                return r.result.items;
            }

            return null;
        }

        string classIDFile = "";

        private void loadClassIDFile(string name)
        {
            if (classIDFile == "")
            {
                classIDFile = new WebClient().DownloadString("http://steamcommunity.com/id/" + name + "/inventory/json/730/2");
            }
        }

        public Int64 ReadClassID(string name, Int64 assetid)
        {
            loadClassIDFile(name);
            
            int start = classIDFile.IndexOf(assetid.ToString());
            string edited = classIDFile.Substring(start + assetid.ToString().Length + 2);
            int stop = edited.IndexOf('}');
            edited = edited.Substring(0, stop+1);

            JavaScriptSerializer js = new JavaScriptSerializer();
            IDs ids = js.Deserialize<IDs>(edited);
            /*if (ids.classid != null)
            {
                return Convert.ToInt64(ids.classid);
            }*/
            return Convert.ToInt64(ids.classid);
        }

        public IDs ReadIDs(string name, Int64 assetid)
        {
            loadClassIDFile(name);

            int start = classIDFile.IndexOf(assetid.ToString());
            string edited = classIDFile.Substring(start + assetid.ToString().Length + 2);
            int stop = edited.IndexOf('}');
            edited = edited.Substring(0, stop + 1);

            JavaScriptSerializer js = new JavaScriptSerializer();
            IDs ids = js.Deserialize<IDs>(edited);
            /*if (ids.classid != null)
            {
                return Convert.ToInt64(ids.classid);
            }*/
            return ids;
        }

        //TODO: json serialization

        public string ReadImageURL(string name, Int64 assetid)
        {
            IDs ids = ReadIDs(name, assetid);
            string objectName = ids.classid.ToString() + "_" + ids.instanceid.ToString();
            int start = classIDFile.IndexOf(objectName);
            string edited = classIDFile.Substring(start + objectName.Length + 4);
            int stop = edited.IndexOf('}');
            edited = edited.Substring(0, stop);
            start = edited.IndexOf("icon_url");
            edited = edited.Substring(start + "icon_url".Length + 3);
            stop = edited.IndexOf('"');
            edited = edited.Substring(0, stop);
            /*if (ids.classid != null)
            {
                return Convert.ToInt64(ids.classid);
            }*/
            return edited;
        }

        public string ReadImageURL(Int64 classid)
        {
            string file = new WebClient().DownloadString("https://api.steampowered.com/ISteamEconomy/GetAssetClassInfo/v1/?key=DCD148674953187B729DEBCC07497510&class_count=1&appid=730&classid0=" + classid.ToString());

            int start = file.IndexOf("icon_url");
            string edited = file.Substring(start + "icon_url".Length + 4);
            int stop = edited.IndexOf('"');
            edited = edited.Substring(0, stop);

            
            /*if (ids.classid != null)
            {
                return Convert.ToInt64(ids.classid);
            }*/
            return edited;
        }

        public string ReadItemName(string name, Int64 assetid)
        {
            IDs ids = ReadIDs(name, assetid);
            string objectName = ids.classid.ToString() + "_" + ids.instanceid.ToString();
            int start = classIDFile.IndexOf(objectName);
            string edited = classIDFile.Substring(start + objectName.Length + 4);
            int stop = edited.IndexOf('}');
            edited = edited.Substring(0, stop);
            start = edited.IndexOf("name");
            edited = edited.Substring(start + "name".Length + 3);
            stop = edited.IndexOf('"');
            edited = edited.Substring(0, stop);
            /*if (ids.classid != null)
            {
                return Convert.ToInt64(ids.classid);
            }*/
            return edited;
        }


    }
    
    

    class IDs
    {
        
        public String classid { get; set; }
        public String instanceid { get; set; }
    }

    class Inentory
    {
        public Result result { get; set; }
        public override string ToString()
        {
            return result.status.ToString();
        }
    }

    class Result
    {
        public int status { get; set; }
        public Item[] items { get; set; }

        public override string ToString()
        {
            return status.ToString();
        }
    }

    class Item
    {
        public Int64 id { get; set; }
        public Int64 original_id { get; set; }
        public Int64 definex { get; set; }
        public Int64 level { get; set; }
        public Int64 quality { get; set; }
        public Int64 rarity { get; set; }
        public Atribute[] attributes { get; set; }

        public double getWare()
        {
            if (this.attributes != null)
            {
                foreach (Atribute a in this.attributes)
                {
                    if (a.defindex == 8)
                    {
                        return a.float_value;
                    }
                }
            }
            return -1;
        }

        public double getPattern()
        {
            if (this.attributes != null)
            {
                foreach (Atribute a in this.attributes)
                {
                    if (a.defindex == 7)
                    {
                        return a.float_value;
                    }
                }
            }
            return -1;
        }
    }

    class Atribute
    {
        public Int64 defindex { get; set; }
        //public int value { get; set; }
        public double float_value { get; set; }
    }
}
