﻿namespace CSGOTraderSystem
{
    partial class ItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelWear = new System.Windows.Forms.Label();
            this.labelExt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(69, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // labelWear
            // 
            this.labelWear.AutoSize = true;
            this.labelWear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelWear.Location = new System.Drawing.Point(0, 49);
            this.labelWear.Name = "labelWear";
            this.labelWear.Size = new System.Drawing.Size(30, 13);
            this.labelWear.TabIndex = 1;
            this.labelWear.Text = "wear";
            // 
            // labelExt
            // 
            this.labelExt.AutoSize = true;
            this.labelExt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelExt.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelExt.Location = new System.Drawing.Point(40, 0);
            this.labelExt.Name = "labelExt";
            this.labelExt.Size = new System.Drawing.Size(29, 15);
            this.labelExt.TabIndex = 2;
            this.labelExt.Text = "MW";
            // 
            // ItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.labelExt);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelWear);
            this.Name = "ItemControl";
            this.Size = new System.Drawing.Size(69, 62);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label labelWear;
        public System.Windows.Forms.Label labelExt;
    }
}
