﻿namespace CSGOTraderSystem
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLink7 = new MetroFramework.Controls.MetroLink();
            this.metroLink6 = new MetroFramework.Controls.MetroLink();
            this.metroLink5 = new MetroFramework.Controls.MetroLink();
            this.metroLink4 = new MetroFramework.Controls.MetroLink();
            this.metroLink3 = new MetroFramework.Controls.MetroLink();
            this.metroLink2 = new MetroFramework.Controls.MetroLink();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(23, 401);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(364, 63);
            this.richTextBox1.TabIndex = 21;
            this.richTextBox1.Text = "https://steamcommunity.com/tradeoffer/new/?partner=83992003&token=y8WLLvN9/";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(23, 379);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(268, 19);
            this.metroLabel18.TabIndex = 20;
            this.metroLabel18.Text = "Support us with skins.(Not necessary)(Kappa)";
            // 
            // metroLink7
            // 
            this.metroLink7.Location = new System.Drawing.Point(23, 295);
            this.metroLink7.Name = "metroLink7";
            this.metroLink7.Size = new System.Drawing.Size(364, 23);
            this.metroLink7.TabIndex = 19;
            this.metroLink7.Text = "http://steamcommunity.com/id/SummerV3/";
            this.metroLink7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLink6
            // 
            this.metroLink6.Location = new System.Drawing.Point(23, 266);
            this.metroLink6.Name = "metroLink6";
            this.metroLink6.Size = new System.Drawing.Size(364, 23);
            this.metroLink6.TabIndex = 18;
            this.metroLink6.Text = "http://steamcommunity.com/id/blejachn1/";
            this.metroLink6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLink5
            // 
            this.metroLink5.Location = new System.Drawing.Point(23, 353);
            this.metroLink5.Name = "metroLink5";
            this.metroLink5.Size = new System.Drawing.Size(364, 23);
            this.metroLink5.TabIndex = 17;
            this.metroLink5.Text = "http://steamcommunity.com/profiles/harambe123123/";
            this.metroLink5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLink4
            // 
            this.metroLink4.Location = new System.Drawing.Point(23, 324);
            this.metroLink4.Name = "metroLink4";
            this.metroLink4.Size = new System.Drawing.Size(364, 23);
            this.metroLink4.TabIndex = 16;
            this.metroLink4.Text = "http://steamcommunity.com/id/chompston/";
            this.metroLink4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLink3
            // 
            this.metroLink3.Location = new System.Drawing.Point(23, 238);
            this.metroLink3.Name = "metroLink3";
            this.metroLink3.Size = new System.Drawing.Size(364, 23);
            this.metroLink3.TabIndex = 15;
            this.metroLink3.Text = "http://steamcommunity.com/id/oorosh/";
            this.metroLink3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLink2
            // 
            this.metroLink2.Location = new System.Drawing.Point(23, 209);
            this.metroLink2.Name = "metroLink2";
            this.metroLink2.Size = new System.Drawing.Size(364, 23);
            this.metroLink2.TabIndex = 14;
            this.metroLink2.Text = "https://www.reddit.com/user/Mand1ch/";
            this.metroLink2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLink1
            // 
            this.metroLink1.Location = new System.Drawing.Point(23, 180);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.Size = new System.Drawing.Size(364, 23);
            this.metroLink1.TabIndex = 13;
            this.metroLink1.Text = "http://steamcommunity.com/id/JuanTheGod/";
            this.metroLink1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(23, 60);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(257, 114);
            this.metroLabel17.TabIndex = 12;
            this.metroLabel17.Text = "Founder, CEO, Content Provider : MandIch\r\nFounder, Developer : OOrosh\r\nGod : Gazd" +
    "aLegi\r\nManager, Reddit Guy: MOLLY-\r\nDesign Provider : Djanki\r\nProud Harambe : Si" +
    "li";
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 478);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.metroLabel18);
            this.Controls.Add(this.metroLink7);
            this.Controls.Add(this.metroLink6);
            this.Controls.Add(this.metroLink5);
            this.Controls.Add(this.metroLink4);
            this.Controls.Add(this.metroLink3);
            this.Controls.Add(this.metroLink2);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.metroLabel17);
            this.Name = "About";
            this.Text = "About";
            this.Load += new System.EventHandler(this.About_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLink metroLink7;
        private MetroFramework.Controls.MetroLink metroLink6;
        private MetroFramework.Controls.MetroLink metroLink5;
        private MetroFramework.Controls.MetroLink metroLink4;
        private MetroFramework.Controls.MetroLink metroLink3;
        private MetroFramework.Controls.MetroLink metroLink2;
        private MetroFramework.Controls.MetroLink metroLink1;
        private MetroFramework.Controls.MetroLabel metroLabel17;
    }
}